import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/AuthService';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
export class NavbarComponent {

  constructor(public authService: AuthService, private router: Router) {
  }

  btnLogout() {
    this.authService.logout().subscribe(
      response => {
        this.router.navigateByUrl('/login');
      }, error => {
        this.router.navigateByUrl('/login');
      });
  }

  btnLogin() {
    this.router.navigateByUrl('/login');
  }

  btnRegister() {
    this.router.navigateByUrl('/register');
  }
}