import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from, of, tap } from 'rxjs';
import Swal from 'sweetalert2'

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private url: String = "http://localhost:8080";
    private tokenKey = 'token';

    constructor(private http: HttpClient) { }

    isAuthenticated(): boolean {
        return !!localStorage.getItem(this.tokenKey);
    }

    isAmdin: boolean = false;

    login(username: string, password: string): Observable<any> {
        const credentials = { username, password };
        return this.http.post(`${this.url}/auth/login`, credentials).pipe(
            tap((response: any) => {
                localStorage.setItem(this.tokenKey, response.message);
                const headers = {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${response.message}`,
                };
                this.http.get<any>(`${this.url}/md`, { headers: headers }).subscribe((response) => {
                    if (response.code === 200 && response.message) {
                        this.isAmdin = response.message.some((role: { name: string; }) => role.name === 'ROLE_ADMIN');

                        if (this.isAmdin) {
                            // Realiza las acciones que necesites aquí
                            console.log('El nombre "ROLE_ADMIN" está presente en la respuesta.');
                        }
                    }
                },
                    (error) => {
                        // Maneja errores aquí
                        console.error('Error al hacer la solicitud:', error);
                    });
            })
        );
    }

    logout(): Observable<any> {
        // Obtener el token del localStorage
        const token = localStorage.getItem(this.tokenKey);
        // Configurar los encabezados con el token
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
        };
        localStorage.removeItem("token");
        return this.http.post(`${this.url}/auth/logout`, {}, { headers: headers });
        // Hacer una solicitud al servidor para invalidar el token

    }

    handleError(message: any) {
        Swal.fire({
            title: 'Error!',
            text: message,
            icon: 'error',
            confirmButtonText: 'Cool'
        });
    }

}
