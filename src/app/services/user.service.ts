import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map } from 'rxjs';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url: String = "http://localhost:8080";
  private tokenKey = 'token';

  constructor(private http: HttpClient) { }

  getRolUser(): Observable<any> {
    const token = localStorage.getItem(this.tokenKey);
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    };
    console.log("Token", token);
    return this.http.get(`${this.url}/list/roles`, { headers: headers });
  }

  getPerson(): Observable<any> {
    const token = localStorage.getItem(this.tokenKey);
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    };
    return this.http.get(`${this.url}/admin/list/person`, { headers: headers });
  }

  getRols(id: number): Observable<any> {
    const token = localStorage.getItem(this.tokenKey);
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    };
    return this.http.get(`${this.url}/admin/rol/acccess?id=${id}`, { headers: headers });
  }

  registerUser(formData: any): Observable<any> {
    return this.http.post(`${this.url}/auth/register`, formData);
  }

  registerUserRol(data: any): Observable<any> {
    const token = localStorage.getItem(this.tokenKey);
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    };
    console.log(data);
    return this.http.post(`${this.url}/admin/rol/acccess/save?userId=${data.userId}&rolId=${data.rolId}`, {}, { headers: headers });
  }

  handleError(message: any) {
    Swal.fire({
      title: 'Error!',
      text: message,
      icon: 'error',
      confirmButtonText: 'Cool'
    });
  }

  handleSucces(message: any) {
    Swal.fire({
      title: message,
      icon: 'success',
      confirmButtonText: 'Cool'
    });
  }

}
