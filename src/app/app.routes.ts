import { Routes } from '@angular/router';
import { AuthComponent } from './components/auth/AuthComponent';
import { UserComponent } from './components/user/user.component';
import { RegisterComponent } from './components/register/register.component';
import { RolComponent } from './components/rol/rol.component';


export const AppRoutes: Routes = [
    { path: 'login', component: AuthComponent },
    { path: 'home', component: UserComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'person', component: RolComponent }
]