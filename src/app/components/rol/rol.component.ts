import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/AuthService';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent {
  dataUser: any[] = [];
  dataRols: any[] = [];
  idUser: number = 0;
  registerForm!: FormGroup;

  constructor(private authService: AuthService, private homeService: UserService, private router: Router, private fb: FormBuilder) {
    if (authService.isAuthenticated()) {
      this.homeService.getPerson().subscribe((response: any) => {
        this.dataUser = response.message;
      }, error => {
        if (error.error.code == 401) {
          this.router.navigateByUrl('/login');
          this.authService.logout();
        } else {
          this.router.navigateByUrl('/home');
        }
        this.authService.handleError(error.error.message);
      });
      return;
    }
    this.router.navigateByUrl('/login');
  }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      rolid: [Validators.required]
    });
  }

  clickOpenModal(iduser: number) {
    this.homeService.getRols(iduser).subscribe(response => {
      this.idUser = iduser;
      this.dataRols = response.message;
    }, error => {
      if (error.error.code == 401) {
        this.router.navigateByUrl('/login');
        this.authService.logout();
      } else {
        this.router.navigateByUrl('/home');
      }
      this.authService.handleError(error.error.message);
    });
  }

  clickSaveRolUser() {
    this.homeService.registerUserRol({ "userId": this.idUser, "rolId": this.registerForm.value.rolid }).subscribe(
      response => {
        this.homeService.handleSucces("Datos guardados correctamentes");
      }
    );
  }

}
