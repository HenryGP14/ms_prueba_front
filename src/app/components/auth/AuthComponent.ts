import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/AuthService';


@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})
export class AuthComponent {

    username: string = "";
    password: string = "";

    loginForm!: FormGroup;

    constructor(private authService: AuthService, private router: Router, private fb: FormBuilder) {
        if (authService.isAuthenticated()) {
            this.router.navigateByUrl('/home');
            return;
        }
        this.router.navigateByUrl('/login');
    }

    ngOnInit(): void {
        this.loginForm = this.initForm();
    }

    onSubmit(): void {
        this.username = this.loginForm.get('username')?.value;
        this.password = this.loginForm.get('password')?.value;
        this.authService.login(this.username, this.password)
            .subscribe(response => {
                if (response.code != 200 && !response.message) {
                    this.authService.handleError(response.message);
                }
                this.router.navigateByUrl('/home');
            }, error => {
                this.authService.handleError(error.error.message);
            });
    }

    initForm(): FormGroup {
        return this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    logout() {
        this.authService.logout().subscribe(
            response => {
                localStorage.removeItem("token");
            }, error => {
                localStorage.removeItem("token");
            });
    }

}
