import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;

  constructor(private fb: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      fullname: ['', Validators.required],
      lastname: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      date: ['', Validators.required],
      cdi: ['', Validators.required]
    });
  }

  onSubmit() {

    const formData = this.registerForm.value;

    this.userService.registerUser(formData).subscribe(
      response => {
        this.userService.handleSucces(response.message);
        this.router.navigateByUrl('/login');
      },
      error => {
        this.userService.handleError(error.error.message);
      }
    );
  }
}
