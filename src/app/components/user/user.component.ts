import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/AuthService';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {

  rolesData: any[] = [];

  constructor(private authService: AuthService, private homeService: UserService, private router: Router) {
    if (authService.isAuthenticated()) {
      this.homeService.getRolUser().subscribe((response: any) => {
        this.router.navigateByUrl('/home');
        this.rolesData = response.result;
      }, error => {
        this.authService.logout();
        this.authService.handleError(error.error.message);
        this.router.navigateByUrl('/login');
      });
      return;
    }
  }
}
